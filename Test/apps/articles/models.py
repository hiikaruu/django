from django.db import models
from django.utils import timezone


class Article(models.Model):
    article_title = models.CharField('Название статьи', max_length=100)
    article_text = models.TextField('Текст статьи')
    publish_date = models.DateField('Дата публикации')

    def __str__(self):
        return self.article_title

    def was_published_resently(self):
        return self.publish_date >= timezone.localdate() - timezone.timedelta(days=7)

    class Meta:
        verbose_name = 'Статья'
        verbose_name_plural = 'Статьи'

class Comment(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    author_name = models.CharField('Имя автора', max_length=70)
    comment_text = models.CharField('Текст комментария', max_length=200)

    def __str__(self):
        return self.author_name

    class Meta:
        verbose_name = 'Коммент'
        verbose_name_plural = 'Комменты'
